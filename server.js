
import express from 'express';
import mongoose from 'mongoose'
//import cors from 'cors';
import {
  graphqlExpress,
  graphiqlExpress,
} from 'graphql-server-express';
import bodyParser from 'body-parser';

import { schema } from './src/schema';
// database connection
const MONGO_URI ='mongodb://tawfik:root12345@ds139969.mlab.com:39969/data'
mongoose.connect(MONGO_URI);
mongoose.set('debug', true)
mongoose.connection
    .once('open', () => console.log('Connected to MongoLab instance.'))
    .on('error', error => console.log('Error connecting to MongoLab:', error));

const server = express()



server.use('/graphql', bodyParser.json(), graphqlExpress({
    schema 
  }));
  
  server.use('/graphiql', graphiqlExpress({
    endpointURL: '/graphql'
  }));
  
  server.listen(4000, () =>
    console.log(`GraphQL Server is now running on http://localhost 3000`)
  );