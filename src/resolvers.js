import UserModel from './models/user'
import PostModel from './models/posts'
export const resolvers = {
    Query: {
        users: () => UserModel.find(),
        /*user: (root, params) => {
            const user = UserModel.findById(params.id).exec();
            if (!user) return null;
            const posts = PostModel.find({ _uid: params.id }).exec();
            return {
                ...user,
                posts
            }


        },*/
        blabla: () => UserModel.findOne(),
        user: (root, { id }) => UserModel.findById(id),
        posts: (root, params) => {
            const posts = PostModel.find().exec()
            if (!posts) {
                throw new Error('post not find')
            }
            return posts
        },
        post: (root, params) => {
            return PostModel.findById(params.id).exec()
        }

    },
    User: {
        id:user=>  user._id,
        posts: user => {
            return PostModel.find({ _uid: user._id })
        }
    },
    Mutation: {
        addUser: (root, params) => {
            const uModel = new UserModel(params);
            const newUser = uModel.save();
            if (!newUser) {
                throw new Error('Error');
            }
            return newUser
        },
        removeUser: (root, params) => {
            const removeduser = UserModel.findByIdAndRemove(params.id).exec();
            if (!removeduser) {
                throw new Error('Error')
            }
            return removeduser;
        },
        updateUser: (root, params) => {
            return UserModel.findByIdAndUpdate(params.id, { $set: { name: params.name, email: params.email } }, { new: true })
                .catch(err => new Error(err))
        },

        addPost: (root, params) => {
            const pModel = new PostModel(params)
            const newPost = pModel.save()
            if (!newPost) {
                throw new Error('error ')
            }
            return newPost

        }
    }

}