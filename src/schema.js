import {

    makeExecutableSchema
    
    } from 'graphql-tools'
    import { resolvers } from "./resolvers";
    const typeDefs= `
    
        type User {
            id: ID              
            name: String
            email:String
            posts:[Post]      
          }
          type Post {
              id:ID!
              _uid:String
              name:String
              body:String
          }
      
          type Query {
            users: [User]    
            user(id: ID!): User
            posts:[Post]
            post(id:ID!):Post
            blabla: Boolean
          }
          type Mutation {
           
            addUser(name: String!,email:String): User
            removeUser(id:ID!):User
            updateUser(id:ID!,name:String,email: String):User
            addPost(_uid:String, name:String,body:String):Post
            
          }
    
    
    
    `
    export const schema = makeExecutableSchema({typeDefs,resolvers})