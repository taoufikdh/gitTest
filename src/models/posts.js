import mongoose from 'mongoose'
var Schema = mongoose.Schema;

var postSchema = new Schema({


_uid :{
    type: Schema.Types.ObjectId  ,
   required:true,
   ref: 'User'
   },
   name:{
       type:String,
       required:true
   },
   body:{
       type:String,
       required:true
   }
})
export default mongoose.model('Post',postSchema)
