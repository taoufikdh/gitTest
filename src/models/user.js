import mongoose from 'mongoose'
var Schema = mongoose.Schema;

var userSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email:{
      type:String
  }
});
var Model = mongoose.model('User', userSchema);
export default Model;
